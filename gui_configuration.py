import os
import utils

class Configuration(object):
    def __init__(self, config_file):
        self.config_file = config_file
        self.csv_output_file = None
        self.csv_append = None
        self.frame_capture = None
        self.detect = None
        self.font_size = None

    def define_settings(self):
        with open(self.config_file, 'r') as f:
            lines = f.readlines()
            for line in lines:
                if line.startswith('#'): # comment in config file
                    continue
                else:
                    cut = line.strip().split('=')
                    if cut[0] == 'csv_output_path':
                        csv_path = cut[1].strip()
                        self.csv_output_file = os.path.join(csv_path, utils.Utils.output_log_file)
                    elif cut[0] == 'csv_append':
                        self.csv_append = eval(cut[1].strip())
                    elif cut[0] == 'frame_capture':
                        self.frame_capture = int(cut[1].strip())
                    elif cut[0] == 'draw_boxes':
                        self.detect = eval(cut[1].strip())
                    elif cut[0] == 'font_size':
                        self.font_size = int(cut[1].strip())