from yolo_container import yoloMethods
from darknet_yolo import YoloContainer
from gui_configuration import Configuration
import tensorflow as tf
import weights_loader
import tkinter
from tkinter.font import Font as font
from tkinter.messagebox import *
from utils import Utils
import cv2
import threading
import time
import os.path
import math

class GUI(tkinter.Frame):
    def __init__(self, master):
        tkinter.Frame.__init__(self,master)
        self.master = master
        # main window settings (background color and size)
        self.master.configure(background=Utils.default_background_color)
        self.screen_width = master.winfo_screenwidth() - 20
        self.screen_height = master.winfo_screenheight() - 20
        self.master.geometry('%sx%s' % (self.screen_width, self.screen_height))
        self.grid()

        # make full screen and add key events
        self.fullscreen = True
        self.master.attributes('-fullscreen', self.fullscreen)
        self.master.bind("<F11>", self.enter_fullscreen)
        self.master.bind("<f>", self.enter_fullscreen)
        self.master.bind("<F>", self.enter_fullscreen)
        self.master.bind("<Escape>", self.exit_fullscreen)
        self.master.bind("<space>", self.space_function)

        # define settings based on external config file
        self.configuration = Configuration(Utils.get_configuration_file())
        self.configuration.define_settings()

        # init tiny yolo
        # define the session
        self.session = tf.InteractiveSession()
        tf.global_variables_initializer().run()
        # get the last checkpoint and load weights (or do it from binary file)
        saver = tf.train.Saver()
        _ = weights_loader.load(self.session, Utils.weights_path, Utils.ckpt_folder_path, saver)

        # init darknet yolo
        # have to edit filepath for 'coco.names' first
        Utils.edit_darknet_yolo_names_path()
        self.darknet_yolo = YoloContainer()

        # declare button/frame sizes
        self.button_width=15
        self.button_height=3
        self.title_height = 100
        self.stream_frame_width=self.screen_width*.73
        self.stream_frame_height=self.screen_height
        self.detail_frame_width = self.screen_width*.22
        self.detail_frame_height = self.screen_height - 75
        # detail log information
        self.detail_log_font_size = self.configuration.font_size  # 16
        self.font_pixel_size = Utils.get_pixel_size_from_font_size(self.detail_log_font_size)
        if self.font_pixel_size < 0:
            showwarning('Font Size Error', 'The font size declared in the configuration file is not allowed, using default size of 18', default=OK, icon=WARNING)
            self.detail_log_font_size = 18
            self.font_pixel_size = Utils.get_pixel_size_from_font_size(18)
        self.detail_log_width = 38#54#38
        #self.detail_log_height = 40#28

        # init stream vars
        self.detect = self.configuration.detect
        self.streaming = False
        self.video_capture = None
        self.stop_event = None
        self.img_canvas = None

        # start video capture
        self.init_video_capture()

        # set radio buttons / adjustables
        self.radio_selection = tkinter.IntVar()
        self.radio_selection.set(2)
        self.slider_selection = tkinter.DoubleVar()
        self.slider_selection.set(Utils.score_threshold)

        # draw frames/canvas'/buttons
        #self.create_title()
        self.create_stream_pane()
        self.create_buttons()
        logo_height = self.add_novetta_logo()
        self.create_detail_pane(logo_height)
        #self.create_buttons()
        #self.add_novetta_logo()

        # init output/log file
        csv_append = os.path.exists(self.configuration.csv_output_file) and self.configuration.csv_append
        self.log_file = open(self.configuration.csv_output_file, 'a' if csv_append else 'w')
        if not csv_append:
            self.log_file.write(Utils.csvFormat(Utils.headers, True))

        # start stream
        self.stop_event = threading.Event()
        self.start_stream()

        # close event
        self.master.wm_protocol('WM_DELETE_WINDOW', self.on_close)
        self.master.bind("<q>", self.on_close)
        self.master.bind("<Q>", self.on_close)

        # self.display_application_help()
        # showinfo('Help', 'Application keys:\n\'Q/q\': Quits application\n\'F/f\': Enters fullscreen mode', icon=INFO, default=OK)

    def display_application_help(self):
        showinfo('Help', 'Application keys:\n\'Q/q\': Quits application\n\'F/f\': Enters fullscreen mode',
                 icon=INFO,
                 default=OK)

    # not used, I want the keys to be used for different things
    def toggle_fullscreen(self, event=None):
        self.fullscreen = not self.fullscreen
        self.master.attributes('-fullscreen', self.fullscreen)
        return 'break'

    def enter_fullscreen(self, event=None):
        if not self.fullscreen:
            self.fullscreen = True
            self.master.attributes('-fullscreen', self.fullscreen)
            return 'break'

    def exit_fullscreen(self, event=None):
        if self.fullscreen:
            self.fullscreen = False
            self.master.attributes('-fullscreen', self.fullscreen)
            return 'break'

    def start_stream(self):
        self.detail_log.config(state='normal')
        self.streaming=True
        self.toggle_button_image()
        self.stream_thread = None
        self.stream_thread = threading.Thread(target=self.live_stream)
        self.stream_thread.start()

    def create_title(self):
        title_frame = tkinter.Frame(self, height=self.title_height, width=self.screen_width, bg=Utils.default_background_color)
        title_frame.grid(columnspan=2, row=0, rowspan=1, sticky=tkinter.W+tkinter.E+tkinter.N)

        title_canvas = tkinter.Canvas(title_frame, width=self.screen_width-5, height=self.title_height, bg=Utils.default_background_color, borderwidth=0, highlightthickness=0)
        title_canvas.grid(column=0, row=0, columnspan=2, sticky=tkinter.W+tkinter.E+tkinter.N, padx=10, pady=10)

        title_font = font(family='Cooper Black', size=56, weight='bold')
        title_canvas.create_text(self.screen_width*.5,100,text='Live Stream Object Detection', anchor=tkinter.S, fill='#154abf', justify=tkinter.CENTER, font=title_font)

    def create_stream_pane(self):
        stream_frame = tkinter.Frame(self, height=self.stream_frame_height, width=self.stream_frame_width,
                                         bg=Utils.default_background_color)#bg='#cccdce')#bg='#474747')
        stream_frame.grid(row=0, column=0, columnspan=10, rowspan=10, sticky=tkinter.N + tkinter.W)

        self.stream_canvas = tkinter.Canvas(stream_frame, width=self.stream_frame_width, highlightthickness=0, bd=0,
                                           height=self.stream_frame_height)# - 100) # moved buttons so removed height adjuster
        self.stream_canvas.grid(column=0, row=0, columnspan=10, padx=15, pady=10)

    def create_detail_pane(self, img_h):
        detail_frame = tkinter.Frame(self, height=self.detail_frame_height+100, width=self.detail_frame_width, bg=Utils.default_background_color, #, bg='#a3474c',
                                          borderwidth=0, highlightthickness=0)
        detail_frame.grid(column=10, row=2, sticky=tkinter.N)

        detail_canvas = tkinter.Canvas(detail_frame, height=self.detail_frame_height, width=self.detail_frame_width, bg=Utils.default_background_color)#, bg='#efc6c9')
        detail_canvas.grid(column=0, row=2)

        # start text stream
        # text sizes in tkinter are based on the font size as opposed to pixel size, have to do some conversion
        # convert height
        remaining_detail_frame_space = self.detail_frame_height - img_h
        log_height = math.floor((remaining_detail_frame_space - 100) / (self.font_pixel_size+4))
        # convert width

        detail_font = font(family='Helvetica', size=self.detail_log_font_size)
        self.detail_log = tkinter.Text(detail_canvas, font=detail_font, height=log_height, width=self.detail_log_width, cursor='X_cursor')
        self.detail_log.grid(pady=10, sticky=tkinter.N+tkinter.E+tkinter.W, rowspan=8)
        detail_scroll_bar = tkinter.Scrollbar(detail_canvas, command=self.detail_log.yview, bg='#ffffff')
        detail_scroll_bar.grid(row=0,column=0, rowspan=8, sticky=tkinter.E+tkinter.N+tkinter.S)
        self.detail_log['yscrollcommand'] = detail_scroll_bar.set
        self.detail_log.insert(0.0, '\n\t----- STREAM STARTED -----\n')

    def create_buttons(self):
        self.button_frame = tkinter.Frame(self, width=self.detail_frame_width, background=Utils.default_background_color,
                                          borderwidth=0, highlightthickness=0, border='0')
        self.button_frame.grid(column=10,row=0, rowspan=2, sticky=tkinter.N+tkinter.S+tkinter.E+tkinter.W)

        # radio buttons for analyzer choice
        radio_font = font(family='Helvetica', size=12)
        tiny_yolo_radio = tkinter.Radiobutton(self.button_frame, font=radio_font, text=Utils.sources[1], bg=Utils.default_background_color,
                                              variable=self.radio_selection, value=1, height=2, borderwidth=0, highlightthickness=0)
        tiny_yolo_radio.grid(padx=5, row=0, column=0, sticky=tkinter.S)#,sticky=tkinter.E+tkinter.W)

        darknet_yolo_radio = tkinter.Radiobutton(self.button_frame, font=radio_font, text=Utils.sources[2], bg=Utils.default_background_color,
                                                 variable=self.radio_selection, value=2, height=2, borderwidth=0, highlightthickness=0)
        darknet_yolo_radio.grid(padx=5, row=1, column=0, sticky=tkinter.N)#, sticky=tkinter.E + tkinter.W)
        darknet_yolo_radio.invoke()

        # slider to adjust threshold
        slider = tkinter.Scale(self.button_frame, orient=tkinter.HORIZONTAL, from_=0.05, to=0.95, label='Threshold',
                               variable=self.slider_selection, length=int(self.detail_frame_width/2), resolution=0.05, bg=Utils.default_background_color)
        slider.grid(row=0, column=2, rowspan=2)#+tkinter.W)

        # buttons for play and stop
        # play_image, w, h = Utils.get_buttons('p')
        # play_button = tkinter.Button(self.button_frame, width=w, height=h, image=play_image, bg=Utils.default_background_color,
        #                                   command=self.play_stream, border='0', highlightthickness=0)
        # play_button.image = play_image
        # play_button.grid(row=0, column=3, padx=2, pady=2, sticky=tkinter.E)#, sticky=tkinter.E+tkinter.W)
        #
        # stop_image, w, h = Utils.get_buttons('s')
        # stop_button = tkinter.Button(self.stream_frame, width=w, height=h, image=stop_image, bg=Utils.default_background_color,
        #                                    command=self.stop_stream, border='0', highlightthickness=0)
        # stop_button.image = stop_image
        #stop_button.grid(row=2,column=9, padx=2, pady=2)#, sticky=tkinter.W)#, sticky=tkinter.E+tkinter.W)

    def toggle_button_image(self):
        # changing image between play and stop based on stream
        command_type = self.stop_stream if self.streaming else self.play_stream
        button_type = 's' if self.streaming else 'p'
        button_image, w, h = Utils.get_buttons(button_type)
        button = tkinter.Button(self.button_frame, width=w, height=h, image=button_image, bg=Utils.default_background_color,
                                command=command_type, border='0', highlightthickness=0)
        button.image = button_image
        button.grid(rowspan=2, row=0, column=1, padx=int(self.detail_frame_width/10)-5, pady=20)#, sticky=tkinter.E)

    def add_novetta_logo(self):
        w = int(self.detail_frame_width) + 30
        logo_frame = tkinter.Frame(self, background=Utils.default_background_color,
                                        borderwidth=0, highlightthickness=0, border='0')
        logo_frame.grid(column=10, row=3, sticky=tkinter.S)#, sticky=tkinter.E + tkinter.W + tkinter.N)

        img, h = Utils.get_logo(w)
        logo = tkinter.Label(logo_frame, image=img, bg=Utils.default_background_color, borderwidth=0, highlightthickness=0, border='0')
        logo.image = img
        logo.grid(pady=5, padx=15, column=0,row=0, sticky=tkinter.W+tkinter.S+tkinter.E + tkinter.N)
        return h

    def show_selection(self):
        print(self.slider_selection.get())

    def init_video_capture(self):
        try:
            if self.video_capture is None or not self.video_capture.isOpened():
                self.video_capture = cv2.VideoCapture(0)
        except(RuntimeError):
            try:
                if self.video_capture is None:
                    self.video_capture = cv2.VideoCapture(0)
                elif not self.video_capture.isOpened():
                    self.video_capture = cv2.VideoCapture(0)
            except(RuntimeError):
                print('cannot open camera')
                showerror('Camera error', 'Unable to open camera, please restart application', icon=ERROR, default=OK)

    def kill_video_capture(self):
        if self.video_capture.isOpened():
            self.video_capture.release()

    def live_stream(self):
        try:
            fps = 0.0
            frame_count = 0
            while self.streaming:
                start = time.time()
                _, frame = self.video_capture.read()
                frame_count += 1
                image = frame
                if frame_count % self.configuration.frame_capture == 0:
                    predictions = None
                    detector_source = self.radio_selection.get()
                    threshold = self.slider_selection.get()

                    if self.detect:
                        #if self.option == 'tiny': # tiny yolo
                        if detector_source == 1:
                            image, predictions = yoloMethods.perform_detection(frame, image, self.session, threshold, frame_count)
                        #elif self.option == 'darknet': # darknet yolo
                        elif detector_source == 2:
                            image, predictions = self.darknet_yolo.analyze_image(frame, threshold, frame_count, Utils.img_name)

                        if predictions is not None:
                            self.append_details(predictions, threshold)
                            # flush csv file every nth frame, determined in Utils (move to config?) # this mofo might autoflush!
                            # if (frame_count % Utils.flush_num) < self.configuration.frame_capture:
                            #     self.log_file.flush()

                    image = Utils.imageConversion(image, self.stream_frame_width, self.stream_frame_height)#-100)

                    if self.img_canvas is None:
                        self.img_canvas = tkinter.Label(self.stream_canvas, image=image)
                        self.img_canvas.image = image
                        self.img_canvas.grid(row=0,column=0,columnspan=10, sticky=tkinter.E+tkinter.S+tkinter.W+tkinter.N)
                    else:
                        self.img_canvas.configure(image=image)
                        self.img_canvas.image=image
                    end = time.time()
                    seconds = end-start
                    fps = (fps + (1/seconds)) / 2
                    print('FPS: ' + str(fps))

        except(RuntimeError):
            print('an error occurred')
            showerror('Stream Error', 'An error occurred while streaming, please restart application', icon=ERROR, default=OK)
            self.kill_video_capture()

    def append_details(self, preds, thresh):
        #self.detail_log.config(state='normal')
        if not bool(preds):
            self.detail_log.insert(0.0, '\n\n\n')
        else:
            dashes = '-'*int(self.detail_log_width*1.5) + '\n'
            self.detail_log.insert(0.0, dashes)
            #for e in preds.keys():
            for e in preds:
                self.detail_log.insert(0.0, e.format_predictions_for_detail_pane())
                #self.detail_log.insert(0.0, Utils.formatDetails(e, Utils.adjustConfidenceScore(preds[e])))
                self.log_file.write(Utils.csvFormat([e.object_name, e.confidence_score, thresh,
                                                    Utils.sources[e.detection_source], e.frame, e.format_bounding_box(True)],
                                                    False))
        #self.detail_log.config(state='disabled')

    # allows play/stop using the spacebar
    def space_function(self, event=None):
        if self.streaming:
            self.stop_stream()
        else:
            self.play_stream()

    def play_stream(self):
        if not self.streaming:
            #self.detail_log.delete('1.0', tkinter.END)
            print('playing')
            if self.img_canvas is not None:
                self.img_canvas.grid_remove()
                self.img_canvas = None
            #self.streaming = True
            self.init_video_capture()
            #self.live_stream()
            self.start_stream()

    def stop_stream(self):
        if self.streaming:
            print('stopping')
            self.detail_log.config(state='disabled')
            self.streaming=False
            self.toggle_button_image()
            self.kill_video_capture()

    def on_close(self, event=None):
        if self.streaming:
            self.stop_stream()
            time.sleep(3)
        self.log_file.close()
        self.master.destroy()

def main():
    try:
        master = tkinter.Tk()
        g = GUI(master)
        g.master.title('ML-COE - Streaming, Deep Learning-Based Object Detection')
        g.mainloop()
    except(RuntimeError):
        showerror('Fatal Error', 'A fatal error occured, please restart application', icon=ERROR, default=OK)

main()