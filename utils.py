import time
import datetime
import cv2
import os
import math
from PIL import Image, ImageTk

class Utils:
    # tiny yolo paths
    weights_path = './tiny-yolo-voc.weights'
    output_image_path = './output.jpg'
    ckpt_folder_path = './ckpt/'

    # image name
    img_name = 'frame.jpg'

    # config file location
    #config_file = '/home/dev-user/Desktop/LiveStreamObjectDetection.config'
    #config_file = './configuration/LiveStreamObjectDetection.config'

    # output log file
    output_log_file = 'objects_detected.csv'
    # csv headers
    headers = ['OBJECT', 'CONFIDENCE SCORE', 'THRESHOLD', 'DETECTION SOURCE', 'FRAME NUMBER', 'BOUNDING BOX CENTER X',
               'BOUNDING BOX CENTER Y', 'BOUNDING BOX WIDTH', 'BOUNDING BOX HEIGHT', 'CAPTURE TIME', 'CAPTURE DATE']
    # flush frame (csv will flush/save every nth frame
    flush_num = 500

    # tiny yolo parameters
    input_height = 416
    input_width = 416
    score_threshold = 0.5 # no longer used, this is now a parameter based on the slider
    iou_threshold = 0.3

    # button info
    play_button = './img/play_button.png'
    stop_button = './img/stop_button.png'
    button_width = 70
    button_height = 50
    default_background_color = '#ffffff'#'#c6c6c6' #white

    # Novetta logo
    novetta_logo = './img/novetta_full_mark_blue_RGB@2x.png'

    # sources
    sources = {1: 'Tiny Yolo V2', 2: 'Yolo V3'}#''Darknet Yolo'}

    # font size > pixel size (hardcoded values as opposed to populating from a file)
    font_size_conversion = {6: 8, 7: 9, 8: 11, 9: 12, 10: 13, 11: 15, 12: 16, 13: 17, 14: 19, 15: 21, 16: 22,
                            17: 23, 18: 24, 20: 26, 22: 29, 24: 32, 26: 35, 27: 36, 28: 37}

    # darknet library location
    darknet_library = './libdarknet.so'

    @staticmethod
    def get_pixel_size_from_font_size(f):
        try:
            return Utils.font_size_conversion[f]
        except(KeyError):
            return -1

    @staticmethod
    def get_darknet_library():
        return os.path.abspath(Utils.darknet_library)

    @staticmethod
    def get_configuration_file():
        # a config file will be on the desktop and in a subdirectory, we have to load the most recent one
        default_config_file = './configuration/LiveStreamObjectDetection.config'
        desktop_config_file = os.path.join(os.path.join(os.path.expanduser('~')), 'Desktop',
                                           'LiveStreamObjectDetection.config')
        desktop_exists = os.path.exists(desktop_config_file)
        if desktop_exists:
            if os.path.getmtime(desktop_config_file) > os.path.getmtime(default_config_file):
                return desktop_config_file
        return default_config_file

    @staticmethod
    def edit_darknet_yolo_names_path():
        # this is a bit hackish but we need to adjust the path name to absolute for darknet to load it
        local_names_path = './darknet_resources/coco.names'
        absolute_names_path = os.path.abspath(local_names_path)

        # open file to read
        coco_data = './darknet_resources/coco.data'
        new_lines = []
        with open(coco_data, 'r') as coco_data_file:
            lines = coco_data_file.readlines()
            for line in lines:
                stripped_line = line.strip()
                if stripped_line.startswith('name'):
                    split_line = stripped_line.split('=')
                    if split_line[1].strip() == absolute_names_path:
                        return
                    line = '{} = {}\n'.format(split_line[0].strip(), absolute_names_path)
                new_lines.append(line)

        # write new file
        with open(coco_data, 'w') as tmp:
            tmp.writelines(new_lines)

    @staticmethod
    def formatDetails(cls, cnf):
        if cls is not None:
            #master.geometry('%sx%s' % (self.screen_width, self.screen_height)) #is this a better way to format?
            s = "\n   {}: {}%\n   Captured at {}\n\n"
            return s.format(cls.upper(),cnf,'{:{hms} {ap} - {mdy}}'.format(datetime.datetime.now(),
                                                                            hms='%I:%M:%S', ap='%p', mdy='%m/%d/%Y'))
    @staticmethod
    def csvFormat(s, h):
        o = ''
        for e in range(len(s)-1):
            o += str(s[e])
            o += ','
        o += str(s[len(s)-1])

        if not h:
            now = datetime.datetime.now()
            o += ','
            o += '{:{hms} {ap}}'.format(now, hms='%I:%M:%S', ap='%p')
            o += ','
            o += '{:{mdy}}'.format(now, mdy='%m-%d-%Y')

        return o+'\n'

    @staticmethod
    def adjustConfidenceScore(c):
        s = str(round(c*100,2))
        return s

    @staticmethod
    def imageConversion(img,w,h):
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = Image.fromarray(img)
        # print original resolution (semi-original...)
        #print(img.width)
        #print(img.height)
        img = img.resize((int(round(w)),int(round(h))), Image.ADAPTIVE)
        # print adjusted resolution (to fit stream window)
        #print(img.width)
        #print(img.height)
        img = ImageTk.PhotoImage(img)
        return img

    @staticmethod
    def get_buttons(t):
        img = Image.open(Utils.play_button if t == 'p' else Utils.stop_button)
        w = Utils.button_width
        h = Utils.button_height
        img = img.resize((w,h),Image.ADAPTIVE)
        return ImageTk.PhotoImage(img), w, h

    @staticmethod
    def get_logo(w):
        img = Image.open(Utils.novetta_logo)
        # get original w,h
        i = ImageTk.PhotoImage(img)
        h = math.ceil((w/i.width()) * i.height())
        # adjust for aspect ratio
        #img = img.resize((w,h), Image.ADAPTIVE)
        img = img.resize((w, h), Image.ANTIALIAS)
        return ImageTk.PhotoImage(img), h
