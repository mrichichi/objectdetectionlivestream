import utils
import datetime

class Predictions(object):
    def __init__(self, obj, confidence_score, bounding_box, frame_number, src):
        self.object_name = obj
        self.confidence_score = confidence_score
        self.bounding_box = bounding_box
        self.frame = frame_number
        self.detection_source = src

    # different for tiny yolo (top left x, top left y, bottom right x, bottom right y)
    # and darkent yolo (center x, center y, width, height)
    # let's provide the darknet yolo data at all times
    def format_bounding_box(self, csv):
        center_x = 0
        center_y = 0
        width = None
        height = None
        if self.detection_source == 1:
            width = self.bounding_box[2] - self.bounding_box[0]
            height = self.bounding_box[3] - self.bounding_box[1]
            center_x = self.bounding_box[0] + (width/2)
            center_y = self.bounding_box[1] + (height/2)
        elif self.detection_source == 2:
            center_x = self.bounding_box[0]
            center_y = self.bounding_box[1]
            width = self.bounding_box[2]
            height = self.bounding_box[3]
        formatted_bounding_box = ''
        if csv:
            formatted_bounding_box = '{}, {}, {}, {}'.format(
                int(center_x), int(center_y), int(width), int(height))
        else:
            formatted_bounding_box = 'Bounding Box: x: {}, y: {}, w: {}, h: {}'.format(
                int(center_x), int(center_y), int(width), int(height))
        return formatted_bounding_box

    def format_predictions_for_detail_pane(self):
        s = ' {}: {}%\n {}\n Frame: {}\n Captured at {}\n\n'
        return s.format(self.object_name.upper(), utils.Utils.adjustConfidenceScore(self.confidence_score),
                        self.format_bounding_box(False), self.frame, '{:{hms} {ap} - {mdy}}'.format(datetime.datetime.now(),
                        hms='%I:%M:%S', ap='%p', mdy='%m/%d/%Y'))

    def format_predictions_for_csv(self):
        pass