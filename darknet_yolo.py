from ctypes import *
import math
import cv2
from predictions import Predictions
from utils import Utils

class IMAGE(Structure):
    _fields_ = [("w", c_int),
                ("h", c_int),
                ("c", c_int),
                ("data", POINTER(c_float))]

class METADATA(Structure):
    _fields_ = [("classes", c_int),
                ("names", POINTER(c_char_p))]

class BOX(Structure):
    _fields_ = [("x", c_float),
                ("y", c_float),
                ("w", c_float),
                ("h", c_float)]

class DETECTION(Structure):
    _fields_ = [("bbox", BOX),
                ("classes", c_int),
                ("prob", POINTER(c_float)),
                ("mask", POINTER(c_float)),
                ("objectness", c_float),
                ("sort_class", c_int)]

#lib = CDLL("/home/dev-user/Documents/darknetYolo/darknet/libdarknet.so", RTLD_GLOBAL)
lib = CDLL(Utils.get_darknet_library(), RTLD_GLOBAL)
lib.network_width.argtypes = [c_void_p]
lib.network_width.restype = c_int
lib.network_height.argtypes = [c_void_p]
lib.network_height.restype = c_int

get_color = lib.get_color
get_color.argtypes = [c_int, c_int, c_int]
get_color.restype = c_float

predict_image = lib.network_predict_image
predict_image.argtypes = [c_void_p, IMAGE]
predict_image.restype = POINTER(c_float)

load_net = lib.load_network
load_net.argtypes = [c_char_p, c_char_p, c_int]
load_net.restype = c_void_p

load_meta = lib.get_metadata
load_meta.argtypes = [c_char_p]
load_meta.restype = METADATA

load_image = lib.load_image_color
load_image.argtypes = [c_char_p, c_int, c_int]
load_image.restype = IMAGE

ipl_to_image = lib.ipl_to_image
ipl_to_image.argtypes = [POINTER(c_char_p)]
ipl_to_image.restype = IMAGE

get_network_boxes = lib.get_network_boxes
get_network_boxes.argtypes = [c_void_p, c_int, c_int, c_float, c_float, POINTER(c_int), c_int, POINTER(c_int)]
get_network_boxes.restype = POINTER(DETECTION)

do_nms_obj = lib.do_nms_obj
do_nms_obj.argtypes = [POINTER(DETECTION), c_int, c_int, c_float]

free_image = lib.free_image
free_image.argtypes = [IMAGE]

free_detections = lib.free_detections
free_detections.argtypes = [POINTER(DETECTION), c_int]

class YoloContainer(object):

    def __init__(self):
        #self.cfg_path = "/home/dev-user/Documents/darknetYolo/darknet/cfg/yolov3.cfg".encode('ascii')
        #self.weights_path = "/home/dev-user/Documents/darknetYolo/darknet/yolov3.weights".encode('ascii')
        #self.meta_path = "/home/dev-user/Documents/darknetYolo/darknet/cfg/coco.data".encode('ascii')
        self.cfg_path = './darknet_resources/yolov3.cfg'.encode('ascii')
        self.weights_path = './darknet_resources/yolov3.weights'.encode('ascii')
        self.meta_path = './darknet_resources/coco.data'.encode('ascii')
        self.object_index = self.prepare_object_lookup()
        self.net = None
        self.meta = None
        self.load_configs()

        self.colors = [[1, 0, 1], [0, 0, 1], [0, 1, 1], [0, 1, 0], [1, 1, 0], [1, 0, 0]]

    def prepare_object_lookup(self):
        object_index = {}
        #a = open('/home/dev-user/Documents/darknetYolo/darknet/data/coco.names', 'r')
        a = open('./darknet_resources/coco.names', 'r')
        b = a.readlines()
        s = 1
        for c in b:
            object_index[c.strip()] = s
            s += 1
        return object_index

    def load_configs(self):
        self.net = load_net(self.cfg_path, self.weights_path, 0)
        self.meta = load_meta(self.meta_path)

    def analyze_image(self, frame, t, frame_num, img_name):
        cv2.imwrite(img_name, frame)
        r = self.detect(self.net, self.meta, img_name.encode('ascii'), t)
        print(r)
        img = self.decorate(frame, r)
        preds = []
        for i in r:
            o = i[0].decode('utf-8')
            preds.append(Predictions(o, i[1], i[2], frame_num, 2))
            #preds[o] = Predictions(o, i[1], i[2], frame_num, 2)
            #preds[o] = i[1]
        return img, preds

    def custom_color(self, c, x, max):
        ratio = (x / max) * 5
        i = int(math.floor(ratio))
        j = int(math.ceil(ratio))
        ratio -= i
        r = (1 - ratio) * self.colors[i][c] + ratio * self.colors[j][c]
        return r * 255

    def color_decoration(self, c, max):
        offset = (c * 123457) % max
        r = self.custom_color(2, offset, max)
        g = self.custom_color(1, offset, max)
        b = self.custom_color(0, offset, max)
        # r = get_color(2, offset, max)*255
        # g = get_color(1, offset, max)*255
        # b = get_color(0, offset, max)*255
        return r, g, b

    def detect(self, net, meta, image, thresh, hier_thresh=.5, nms=.45):
        im = load_image(image, 0, 0)
        # im = image
        num = c_int(0)
        pnum = pointer(num)
        predict_image(net, im)
        dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, None, 0, pnum)
        num = pnum[0]
        if (nms): do_nms_obj(dets, num, meta.classes, nms)

        res = []
        for j in range(num):
            for i in range(meta.classes):
                if dets[j].prob[i] > 0:
                    b = dets[j].bbox
                    res.append((meta.names[i], dets[j].prob[i], (b.x, b.y, b.w, b.h)))
        res = sorted(res, key=lambda x: -x[1])
        free_image(im)
        free_detections(dets, num)
        return res

    def decorate(self, img, r):
        # img = cv2.resize(img, (input_height, input_width), interpolation=cv2.INTER_CUBIC)
        for i in r:
            print(i)
            obj = i[0].decode("utf-8")
            print(obj)
            # img = cv2.rectangle(img, (round(i[2][0]), round(i[2][1])), (round(i[2][2]), round(i[2][3])), (255,0,0))
            x1 = int(i[2][0] - int(i[2][2] / 2))
            y1 = int(i[2][1] - int(i[2][3] / 2))
            x2 = x1 + int(i[2][2])
            y2 = y1 + int(i[2][3])
            r, g, b = self.color_decoration(self.object_index[obj], 80)
            img = cv2.rectangle(img, (x1, y1), (x2, y2), (b, g, r), 3)  # bgr for color
            # text above bounding box
            # img = cv2.rectangle(img, (x1, y1 - 25), (x1 + (len(obj) * 10) + 6, y1), (b, g, r), cv2.FILLED)
            # cv2.putText(img, obj, (x1 + 2, y1 - 7), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 0), 2)
            # text within bounding box
            img = cv2.rectangle(img, (x1, y1), (x1+(len(obj)*10)+6,y1+25), (b,g,r), cv2.FILLED)
            cv2.putText(img, obj, (x1 + 4, y1 + 16), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 0), 2)
        return img